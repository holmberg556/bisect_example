#!/bin/zsh -f

for i in $(seq 1 1000) ; do
    cat > hello.py <<EOF

import time

time.sleep(3)

i = $i

if i < 314:
   print()
   print(f'                    hello {i} !!!!!!!!!!!!!!!!!!!!!!!')
   print()
else:
   raise RuntimeError('                 BOOM !!!!!!!!!!!!!!!!')

EOF
    git add hello.py
    git commit -m"say: hello $i"
done
